﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ToyStore.DTO.Main.Toys
{
    public class GetToysDto
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public double Price { get; set; }
        public int Count { get; set; }
        public string ImagPath { get; set; }
        public int MinAge { get; set; }
        public int MaxAge { get; set; }
        public double Evaluation { get; set; }
        public double Discount { get; set; }
        public string Description { get; set; }

        public Guid StorSectionId { get; set; }
        public string StoreName { get; set; }

        public Guid StoreId { get; set; }
        public Guid SectionId { get; set; }
    }
}
