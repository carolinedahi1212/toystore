﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ToyStore.DTO.Main.Toys
{
    public class SetToysDto
    {
        public string Name { get; set; }
        public double Price { get; set; }
        public int Coount { get; set; }
        public IFormFile Image { get; set; }
        public int MinAge { get; set; }
        public int MaxAge { get; set; }
        public string Description { get; set; }
        public List<IFormFile> Images { get; set; }
        public Guid StorId { get; set; }
        public Guid SectionId { get; set; }
    }
}
