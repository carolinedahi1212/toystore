﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ToyStore.DTO.Main.ContactUs
{
    public class GetContactDto
    {
        public Guid Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public DateTime DateCreated { get; set; }
        public bool IsResponsed { get; set; }
        public Guid? OrderId { get; set; }
        public string NumOfOrder { get; set; }
    }
}
