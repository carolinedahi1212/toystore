﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ToyStore.DTO.Main.ContactUs
{
    public class SetContactDto
    {
        public string Title { get; set; }
        public string Description { get; set; }
        public Guid? OrderId { get; set; }
        public Guid SenderId { get; set; }
    }
}
