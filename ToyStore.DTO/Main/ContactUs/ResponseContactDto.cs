﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ToyStore.DTO.Main.ContactUs
{
    public class ResponseContactDto
    {
        public Guid Id { get; set; }
        public string Reply { get; set; }
        public Guid ReplaierId { get; set; }
    }
}
