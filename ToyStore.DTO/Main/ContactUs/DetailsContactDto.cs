﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ToyStore.DTO.Main.ContactUs
{
    public class DetailsContactDto : GetContactDto
    {
        public Guid SenderId { get; set; }
        public string SenderName { get; set; }
        public string Reply { get; set; }
        public DateTime? ReplyDate { get; set; }
    }
}
