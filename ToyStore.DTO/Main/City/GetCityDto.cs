﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ToyStore.DTO.Main.City
{
    public class GetCityDto
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
    }
}
