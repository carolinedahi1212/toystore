﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ToyStore.DTO.Main.Discount
{
    public class GetDiscountDto
    {
        public Guid Id { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public Guid Percentage { get; set; }

        public Guid ToyId { get; set; }
    }
}
