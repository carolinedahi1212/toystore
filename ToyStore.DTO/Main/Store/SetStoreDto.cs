﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ToyStore.DTO.Main.Store
{
    public class SetStoreDto
    {
        public string Name { get; set; }
        public IFormFile Image { get; set; }
        public Guid ManagerId { get; set; }

        public IEnumerable<Guid> SectionIds { get; set; }
    }
}
