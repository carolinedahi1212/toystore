﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ToyStore.DTO.Main.Section;

namespace ToyStore.DTO.Main.Store
{
    public class UpdateStoreDto
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string ImagePath { get; set; }
        public Guid ManagerId { get; set; }
        public IEnumerable<GetSectionDto> SectionId { get; set; }
    }
}
