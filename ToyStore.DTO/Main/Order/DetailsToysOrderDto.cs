﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ToyStore.DTO.Main.Toys;

namespace ToyStore.DTO.Main.Order
{
    public class DetailsToysOrderDto :GetToysDto
    {
        public int ToyCount { get; set; }

    }
}
