﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ToyStore.DTO.Main.Order
{
    public class EvaluateOrderDto
    {
        public int OrderId { get; set; }
        public List<EvaluateToyDto> EvaluateToys { get; set; }
    }

    public class EvaluateToyDto
    {
        public Guid ToyId { get; set; }
        public int Evaluation { get; set; }
    }
}
