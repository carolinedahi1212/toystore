﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ToyStore.DTO.Main.Toys;
using ToyStore.SharedKernel.Enums.Main;

namespace ToyStore.DTO.Main.Order
{
    public class GetOrderDto
    {
        public Guid Id { get; set; }
        public string Num { get; set; }
        public DateTime Date { get; set; }
        public OrderStatus StatusOrder { get; set; }
        public double TotalPrice { get; set; }

        public Guid UserId { get; set; }
        public string UserName { get; set; }

        public string StoreName { get; set; }

        public IEnumerable<DetailsToysOrderDto> ToyOrders { get; set; }
    }
}
