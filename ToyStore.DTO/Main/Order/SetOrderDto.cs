﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ToyStore.SharedKernel.Enums.Main;

namespace ToyStore.DTO.Main.Order
{
    public class SetOrderDto
    {
        public double TotalPrice { get; set; }
        public Guid UserId { get; set; }
        public IEnumerable<ToyOrderDto> Toys { get; set; }
    }

    public class ToyOrderDto
    {
        public Guid ToyId { get; set; }
        public int ToyCount { get; set;}
        public Guid StoreId { get; set; }
        public double FinalPrice { get; set; }
    }
}
