﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ToyStore.DTO.Main.Section
{
    public class SetSectionDto
    {
        public string Name { get; set; }
    }
}
