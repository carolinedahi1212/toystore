﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ToyStore.DTO.Main.Toys;

namespace ToyStore.DTO.Main.Section
{
    public class DetailsSectionDto : GetSectionDto
    {
        public List<GetToysDto> Toys { get; set; }
    }
}
