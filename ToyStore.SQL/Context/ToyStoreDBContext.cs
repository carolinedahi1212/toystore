﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using ToyStore.Models.Base;
using ToyStore.Models.Main;
using ToyStore.Models.Security;
using ToyStore.Models.Shared;

namespace ToyStore.SQL.Context
{
    public class ToyStoreDBContext : IdentityDbContext<User, IdentityRole<Guid>, Guid,
                                                             IdentityUserClaim<Guid>, IdentityUserRole<Guid>,
                                                             IdentityUserLogin<Guid>, IdentityRoleClaim<Guid>,
                                                             IdentityUserToken<Guid>>
    {
        private readonly IHttpContextAccessor httpContextAccessor;

        public ToyStoreDBContext(DbContextOptions options, IHttpContextAccessor httpContextAccessor) : base(options)
        {
            this.httpContextAccessor = httpContextAccessor;
        }

        public DbSet<ContactUs> ContactUs { get; set; }
        public DbSet<Discount> Discounts { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<Section> Sections { get; set; }
        public DbSet<Store> Stores { get; set; }
        public DbSet<StorSection> StorSections { get; set; }
        public DbSet<Toy> Toys { get; set; }
        public DbSet<ToyOrder> ToyOrders { get; set; }
        public DbSet<City> Cities { get; set; }
        public DbSet<CommonQuestion> CommonQuestions { get; set; }

        public DbSet<_File> Files { get; set; }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<ContactUs>()
                        .HasOne(c => c.Sender)
                        .WithMany(c => c.ContactUsSends)
                        .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<ContactUs>()
                        .HasOne(c => c.Replaier)
                        .WithMany(c => c.ContactUsReplaies)
                        .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<ContactUs>()
               .HasOne(b => b.Order)
               .WithOne(i => i.Complaint)
               .HasForeignKey<Order>(o => o.ComplaintId);

            modelBuilder.Entity<ToyOrder>()
                       .HasOne(c => c.Order)
                       .WithMany(c => c.ToyOrders)
                       .OnDelete(DeleteBehavior.Restrict);
        }

        protected bool IsLogged()
        {
            if (httpContextAccessor?.HttpContext?.User != null)
                return httpContextAccessor?.HttpContext?.User?.Identity?.IsAuthenticated ?? false;
            return false;
        }

        public Guid CurrentUserId()
        {
            var userId = Guid.Empty;
            if (IsLogged())
            {
                userId = Guid.Parse(httpContextAccessor.HttpContext.User.Claims.First(claim => claim.Type == ClaimTypes.NameIdentifier)?.Value);
            }
            return userId;
        }

        protected virtual void BeforeSaveChanges()
        {
            foreach (EntityEntry entry in ChangeTracker.Entries())
            {
                var entity = entry.Entity as BaseEntity;
                if (entity is null) continue;
                var actionType = entry.State.ToString();

                switch (entry.State)
                {
                    case EntityState.Detached:
                        break;

                    case EntityState.Unchanged:
                        break;

                    case EntityState.Deleted:
                        break;

                    case EntityState.Modified:
                        break;

                    case EntityState.Added:
                        entity.DateCreated = DateTime.Now.ToLocalTime();
                        break;
                }

            }
        }
    }
}
