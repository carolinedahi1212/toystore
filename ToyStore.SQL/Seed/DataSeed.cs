﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics.Metrics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ToyStore.Models.Main;
using ToyStore.SQL.Context;

namespace ToyStore.SQL.Seed
{
    public class DataSeed
    {
        public static async Task InitializeAsync(IServiceProvider services)
        {
            var context = (ToyStoreDBContext)services.GetService(typeof(ToyStoreDBContext));
            using (var transaction = context.Database.BeginTransaction())
            {
                try
                {
                    await CitySeed(context);
                    await transaction.CommitAsync();
                }
                catch (Exception ex)
                {
                    await transaction.RollbackAsync();
                }
            }
        }

        private static async Task CitySeed(ToyStoreDBContext context)
        {
            if (!context.Cities.Any())
            {
                var city = new City
                {
                    Name = "Aleppo"
                };
                context.Add(city);

                city = new City
                {
                    Name = "Damascus"
                };
                context.Add(city);

                city = new City
                {
                    Name = "Daraa"
                };
                context.Add(city);

                city = new City
                {
                    Name = "DerElzor"
                };
                context.Add(city);

                city = new City
                {
                    Name = "Hama"
                };
                context.Add(city);

                city = new City
                {
                    Name = "AlHasaka"
                };
                context.Add(city);

                city = new City
                {
                    Name = "Homs"
                };
                context.Add(city);

                city = new City
                {
                    Name = "Idlib"
                };
                context.Add(city);

                city = new City
                {
                    Name = "Latakia"
                };
                context.Add(city);

                city = new City
                {
                    Name = "Quneitra"
                };
                context.Add(city);

                city = new City
                {
                    Name = "AlRaqqah"
                };
                context.Add(city);

                city = new City
                {
                    Name = "AlSuwayda"
                };
                context.Add(city);

                city = new City
                {
                    Name = "Tartus"
                };
                context.Add(city);

                await context.SaveChangesAsync();
            }
        }
    }
}
