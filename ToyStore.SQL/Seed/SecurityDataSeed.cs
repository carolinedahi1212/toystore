﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ToyStore.Models.Security;
using ToyStore.SharedKernel.Enums.Security;
using ToyStore.SQL.Context;

namespace ToyStore.SQL.Seed
{
    public class SecurityDataSeed
    {
        public static async Task InitializeAsync(IServiceProvider services)
        {
            var roleManager = (RoleManager<IdentityRole<Guid>>)services.GetService(typeof(RoleManager<IdentityRole<Guid>>));
            var userManager = (UserManager<User>)services.GetService(typeof(UserManager<User>));
            var context = (ToyStoreDBContext)services.GetService(typeof(ToyStoreDBContext));

            var newRoles = await CreateNewRoles(roleManager);
            await CreateSuperAdmin(context, userManager, roleManager);
            return;
        }

        private static async Task<IEnumerable<string>> CreateNewRoles(RoleManager<IdentityRole<Guid>> roleManager)
        {
            var IdentityRoles = Enum.GetValues(typeof(ToyStoreRole)).Cast<ToyStoreRole>().Select(a => a.ToString());
            var ExistedRoles = roleManager.Roles.Select(a => a.Name).ToList();
            var newRoles = IdentityRoles.Except(ExistedRoles);

            foreach (var @new in newRoles)
            {
                await roleManager.CreateAsync(new IdentityRole<Guid>() { Name = @new });
            }

            return newRoles;
        }

        private static async Task CreateSuperAdmin(ToyStoreDBContext context, UserManager<User> userManager, RoleManager<IdentityRole<Guid>> roleManager)
        {
            var superAdmin = await userManager.FindByNameAsync("super");

            if (superAdmin is null)
            {
                superAdmin = new User
                {
                    FirstName = "Super Admin",
                    LastName = "",
                    UserName = "super",
                    UserType = UserType.SuperAdmin,
                    PhoneNumber = "0988888888",
                    Email = "super@gmail.com",
                    GenerationStamp = "",
                    CityId = context.Cities.FirstOrDefault().Id
                };

                var createResult = await userManager.CreateAsync(superAdmin, "1234");

                if (createResult.Succeeded && createResult.Succeeded)
                {
                    var identityRoles = roleManager.Roles.Select(a => a.Name).ToList();
                    var roleResult = await userManager.AddToRolesAsync(superAdmin, identityRoles);
                    if (roleResult.Succeeded)
                        return;
                    throw new Exception(string.Join("\n", roleResult.Errors.Select(error => error.Description)));
                }
                throw new Exception(string.Join("\n", createResult.Errors.Select(error => error.Description)));

            }
        }
    }
}
