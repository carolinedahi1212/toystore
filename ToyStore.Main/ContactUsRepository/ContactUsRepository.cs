﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ToyStore.Base;
using ToyStore.DTO.Main.ContactUs;
using ToyStore.Models.Main;
using ToyStore.SharedKernel.Enums.Security;
using ToyStore.SharedKernel.OperationResult;
using ToyStore.SQL.Context;

namespace ToyStore.Main.ContactUsRepository
{
    public class ContactUsRepository : BaseReposiotry, IContactUsRepository
    {
        public ContactUsRepository(ToyStoreDBContext context) : base(context)
        {
        }

        public async Task<OperationResult<bool>> Create(SetContactDto contactUsDto)
        {
            var operation = new OperationResult<bool>();
            try
            {
                var contactUs = new ContactUs
                {
                    Title = contactUsDto.Title,
                    Description = contactUsDto.Description,
                    SenderId = contactUsDto.SenderId,
                    OrderId = contactUsDto.OrderId
                };
                context.Add(contactUs);
                await context.SaveChangesAsync();

                return operation.SetSuccess(true);
            }
            catch (Exception ex)
            {
                return operation.SetException(ex);
            }
        }

        public async Task<OperationResult<bool>> Delete(Guid id)
        {
            var operation = new OperationResult<bool>();
            try
            {
                var contactUs = await context.ContactUs.Where(c => c.Id.Equals(id)).SingleOrDefaultAsync();

                context.Remove(contactUs);
                await context.SaveChangesAsync();
                return operation.SetSuccess(true);
            }
            catch (Exception ex)
            {
                return operation.SetException(ex);
            }
        }

        public async Task<OperationResult<List<GetContactDto>>> GetAll()
        {
            var operation = new OperationResult<List<GetContactDto>>();
            try
            {
                var contacts = await context.ContactUs.Select(c => new GetContactDto
                {
                    Id = c.Id,
                    Title = c.Title,
                    Description = c.Description,
                    IsResponsed = c.ReplyDate.HasValue,
                    DateCreated = c.DateCreated,
                    OrderId = c.OrderId,
                    NumOfOrder = c.Order!.Num,
                }).ToListAsync();

                return operation.SetSuccess(contacts);
            }
            catch (Exception ex)
            {
                return operation.SetException(ex);
            }
        }

        public async Task<OperationResult<DetailsContactDto>> GetById(Guid id)
        {
            var operation = new OperationResult<DetailsContactDto>();
            try
            {
                var contact = await context.ContactUs.Where(c => c.Id.Equals(id))
                                                     .Select(c => new DetailsContactDto
                                                     {
                                                         Id = c.Id,
                                                         Title = c.Title,
                                                         Description = c.Description,
                                                         IsResponsed = c.ReplyDate.HasValue,
                                                         DateCreated = c.DateCreated,
                                                         Reply = c.Reply,
                                                         ReplyDate = c.ReplyDate,
                                                         SenderId = c.SenderId,
                                                         SenderName = c.Sender.FirstName + " " + c.Sender.LastName,
                                                         NumOfOrder = c.Order!.Num,
                                                         OrderId = c.OrderId
                                                     }).SingleOrDefaultAsync();

                return operation.SetSuccess(contact);
            }
            catch (Exception ex)
            {
                return operation.SetException(ex);
            }
        }

        public async Task<OperationResult<bool>> Response(ResponseContactDto contactUsDto)
        {
            var operation = new OperationResult<bool>();
            try
            {
                var contactUs = await context.ContactUs.Where(c => c.Id.Equals(contactUsDto.Id)).SingleOrDefaultAsync();
                contactUs.ReplaierId = contactUsDto.ReplaierId;
                contactUs.Reply = contactUsDto.Reply;
                contactUs.ReplyDate = DateTime.Now;
                context.Update(contactUs);
                await context.SaveChangesAsync();

                return operation.SetSuccess(true);
            }
            catch (Exception ex)
            {
                return operation.SetException(ex);
            }
        }
    }
}
