﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ToyStore.DTO.Main.ContactUs;
using ToyStore.SharedKernel.OperationResult;

namespace ToyStore.Main.ContactUsRepository
{
    public interface IContactUsRepository
    {
        Task<OperationResult<List<GetContactDto>>> GetAll();
        Task<OperationResult<DetailsContactDto>> GetById(Guid id);
        Task<OperationResult<bool>> Create(SetContactDto contactUsDto);
        Task<OperationResult<bool>> Delete(Guid id);
        Task<OperationResult<bool>> Response(ResponseContactDto contactUsDto);
    }
}
