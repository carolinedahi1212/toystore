﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ToyStore.DTO.Main.Section;
using ToyStore.SharedKernel.OperationResult;

namespace ToyStore.Main.SectionRepository
{
    public interface ISectionRepository
    {
        Task<OperationResult<IEnumerable<GetSectionDto>>> GetAll();
        Task<OperationResult<GetSectionDto>> GetById(Guid id);
        Task<OperationResult<IEnumerable<GetSectionDto>>> GetByStoreId(Guid storeId);
        Task<OperationResult<GetSectionDto>> Create(SetSectionDto sectionDto);
        Task<OperationResult<GetSectionDto>> Update(UpdateSectionDto updateSectionDto);
        Task<OperationResult<bool>> Delete(Guid id);
    }
}
