﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ToyStore.Base;
using ToyStore.DTO.Main.Section;
using ToyStore.DTO.Main.Store;
using ToyStore.Models.Main;
using ToyStore.SharedKernel.OperationResult;
using ToyStore.SQL.Context;

namespace ToyStore.Main.SectionRepository
{
    public class SectionRepository : BaseReposiotry, ISectionRepository
    {
        public SectionRepository(ToyStoreDBContext context) : base(context)
        {
        }

        public async Task<OperationResult<GetSectionDto>> Create(SetSectionDto sectionDto)
        {
            var operation = new OperationResult<GetSectionDto>();
            using (var transaction = context.Database.BeginTransaction())
            {
                try
                {
                    var section = new Section
                    {
                        Name = sectionDto.Name
                    };
                    context.Add(section);
                    await context.SaveChangesAsync();
                    transaction.Commit();

                    return await GetById(section.Id);
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    return operation.SetException(ex);
                }
            }

        }

        public async Task<OperationResult<IEnumerable<GetSectionDto>>> GetAll()
        {
            var operation = new OperationResult<IEnumerable<GetSectionDto>>();

            try
            {
                var sections = await context.Sections
                                            .Select(sec => new GetSectionDto
                                            {
                                                Id = sec.Id,
                                                Name = sec.Name
                                            }).ToListAsync();

                return operation.SetSuccess(sections);
            }
            catch(Exception ex)
            {
                return operation.SetException(ex);
            }
        }

        public async Task<OperationResult<GetSectionDto>> GetById(Guid id)
        {
            var operation = new OperationResult<GetSectionDto>();
            try
            {
                var section = await context.Sections.Where(section => section.Id.Equals(id))
                                                    .Select(seciton => new GetSectionDto
                                                    {
                                                        Id = seciton.Id,
                                                        Name = seciton.Name,
                                                    }).SingleOrDefaultAsync();

                operation.SetSuccess(section);
            }
            catch (Exception ex)
            {
                operation.SetException(ex);
            }
            return operation;
        }

        public async Task<OperationResult<bool>> Delete(Guid id)
        {
            var operation = new OperationResult<bool>();
            try
            {
                var section = await context.Sections.Where(sec => sec.Id.Equals(id))
                                                    .Include(sec => sec.StorSections)
                                                    .SingleOrDefaultAsync();
                context.RemoveRange(section.StorSections);
                context.Remove(section);
                await context.SaveChangesAsync();

                return operation.SetSuccess(true);
            }
            catch (Exception ex)
            {
                return operation.SetException(ex);
            }
        }

        public async Task<OperationResult<GetSectionDto>> Update(UpdateSectionDto updateSectionDto)
        {
            var operation = new OperationResult<GetSectionDto>();
            using (var transaction = context.Database.BeginTransaction())
            {
                try
                {
                    var section = await context.Sections
                                               .Where(section => updateSectionDto.Id.Equals(section.Id))
                                               .SingleOrDefaultAsync();

                    if (section is null)
                    {
                        operation.SetFailed($"this section with {section.Id} id not found.");
                        return operation;
                    }

                    section.Name = updateSectionDto.Name;

                    context.Update(section);
                    await context.SaveChangesAsync();

                    var sectionRes = await GetById(section.Id);
                    operation.SetSuccess(sectionRes.Result);

                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    operation.SetException(ex);
                }
            }
            return operation;
        }

        public async Task<OperationResult<IEnumerable<GetSectionDto>>> GetByStoreId(Guid storeId)
        {
            var operation = new OperationResult<IEnumerable<GetSectionDto>>();
            try
            {
                var section = await context.StorSections.Where(store => store.StoreId.Equals(storeId))
                                                        .Include(sec => sec.Section)
                                                        .Select(sec => new GetSectionDto
                                                        {
                                                            Id = sec.Id,
                                                            Name = sec.Section.Name
                                                        }).ToListAsync();

                return operation.SetSuccess(section);
            }
            catch (Exception ex)
            {
                return operation.SetException(ex);
            }
        }

    }

}
