﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ToyStore.DTO.Main.Order;
using ToyStore.SharedKernel.OperationResult;

namespace ToyStore.Main.OrderRepository
{
    public interface IOrderRepository
    {
        Task<OperationResult<bool>> Create(SetOrderDto orderDto);

        Task<OperationResult<IEnumerable<GetOrderDto>>> GetAll();
        Task<OperationResult<GetOrderDto>> GetById(Guid id);
        Task<OperationResult<IEnumerable<GetOrderDto>>> GetByStoreId(Guid storeID);

        Task<OperationResult<bool>> OnWayOrder(Guid id);
        Task<OperationResult<bool>> FinishOrder(Guid id);
        Task<OperationResult<bool>> EvaluateOrder(EvaluateOrderDto evaluateDto);
    }
}
