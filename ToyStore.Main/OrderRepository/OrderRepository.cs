﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ToyStore.Base;
using ToyStore.DTO.Main.City;
using ToyStore.DTO.Main.Order;
using ToyStore.SharedKernel.OperationResult;
using ToyStore.DTO.Main.Order;
using ToyStore.SharedKernel.OperationResult;
using ToyStore.SQL.Context;
using static System.Formats.Asn1.AsnWriter;
using ToyStore.Models.Main;
using ToyStore.SharedKernel.Enums.Main;
using ToyStore.DTO.Main.Toys;

namespace ToyStore.Main.OrderRepository
{
    public class OrderRepository : BaseReposiotry, IOrderRepository
    {
        public OrderRepository(ToyStoreDBContext context) : base(context)
        {
        }

        public async Task<OperationResult<IEnumerable<GetOrderDto>>> GetAll()
        {
            var operation = new OperationResult<IEnumerable<GetOrderDto>>();

            try
            {
                var orders = await context.Orders
                                          .Select(order => new GetOrderDto
                                          {
                                              Id = order.Id,
                                              Num=order.Num,
                                              Date=order.Date,
                                              StatusOrder=order.StatusOrder,
                                              UserName=order.User.UserName,
                                              UserId=order.UserId,
                                              TotalPrice=order.TotalPrice,
                                          }).ToListAsync();

                return operation.SetSuccess(orders);
            }
            catch (Exception ex)
            {
                return operation.SetException(ex);
            }
        }

        public async Task<OperationResult<GetOrderDto>> GetById(Guid id)
        {
            var operation = new OperationResult<GetOrderDto>();
            try
            {
                var order = await context.Orders.Where(order => order.Id.Equals(id))
                                                .Select(order => new GetOrderDto
                                                {
                                                    Id = order.Id,
                                                    Num = order.Num,
                                                    Date = order.Date,
                                                    StatusOrder = order.StatusOrder,
                                                    UserName = order.User.UserName,
                                                    UserId = order.UserId,
                                                    TotalPrice = order.TotalPrice,
                                                    StoreName=order.ToyOrders.First().Toy.StorSection.Store.Name,
                                                    ToyOrders=order.ToyOrders.Select(toy=>new DetailsToysOrderDto
                                                    {
                                                        ToyCount=toy.ToyCount,
                                                        Id=toy.Toy.Id,
                                                        Name=toy.Toy.Name,
                                                        Price=toy.FinalPrice,
                                                        MaxAge=toy.Toy.MaxAge,
                                                        MinAge=toy.Toy.MinAge,
                                                        ImagPath=toy.Toy.ImagPath,
                                                    }).ToList()
                                                }).SingleOrDefaultAsync();

                operation.SetSuccess(order);
            }
            catch (Exception ex)
            {
                operation.SetException(ex);
            }
            return operation;
        }

        public async Task<OperationResult<bool>> Create(SetOrderDto orderDto)
        {
            var operation = new OperationResult<bool>();
            try
            {
                var toys = orderDto.Toys.GroupBy(x => x.StoreId).ToList();
                var num = GenerateUniqueOrderNumber(context);
                toys.ForEach(toy =>
                {
                    var order = new Order
                    {
                        StatusOrder = OrderStatus.InProcess,
                        Date = DateTime.Now,
                        Num = num,
                        UserId = orderDto.UserId,
                        TotalPrice = orderDto.TotalPrice,
                        ToyOrders = toy.Select(x => new ToyOrder
                        {
                            ToyCount = x.ToyCount,
                            ToyId = x.ToyId,
                            FinalPrice = x.FinalPrice,
                        }).ToList(),
                    };
                    context.Add(order);
                });

                orderDto.Toys.ToList().ForEach(t =>
                {
                    var toy = context.Toys.Where(x=>x.Id.Equals(t.ToyId)).SingleOrDefault();
                    toy.Count -= t.ToyCount;
                    context.Update(toy);
                });
                
                await context.SaveChangesAsync();
                return operation.SetSuccess(true);
            }
            catch (Exception ex)
            {
                return operation.SetException(ex);
            }
        }

        public async Task<OperationResult<bool>> OnWayOrder(Guid id)
        {
            var operation = new OperationResult<bool>();
            try
            {
                var order = await context.Orders.FindAsync(id);
                order.StatusOrder = OrderStatus.OnWay;
                context.Update(order);
                await context.SaveChangesAsync();
                return operation.SetSuccess(true);
            }
            catch (Exception ex)
            {
                return operation.SetException(ex);
            }
        }

        public async Task<OperationResult<bool>> FinishOrder(Guid id)
        {
            var operation = new OperationResult<bool>();
            try
            {
                var order = await context.Orders.FindAsync(id);
                order.StatusOrder = OrderStatus.Received;
                context.Update(order);
                await context.SaveChangesAsync();
                return operation.SetSuccess(true);
            }
            catch (Exception ex)
            {
                return operation.SetException(ex);
            }
        }

        public async Task<OperationResult<bool>> EvaluateOrder(EvaluateOrderDto evaluateDto)
        {
            var operation = new OperationResult<bool>();
            try
            {
                var order = await context.Orders.Where(o => o.Id.Equals(evaluateDto.OrderId))
                                                .Include(o => o.ToyOrders)
                                                .SingleOrDefaultAsync();
                evaluateDto.EvaluateToys.ForEach(t =>
                {
                    var toy = order.ToyOrders.Where(to => to.ToyId.Equals(t.ToyId)).SingleOrDefault();
                    toy.Evaluation = t.Evaluation;
                    context.Update(toy);
                });
                await context.SaveChangesAsync();
                return operation.SetSuccess(true);
            }
            catch (Exception ex)
            {
                return operation.SetException(ex);
            }
        }

        public async Task<OperationResult<IEnumerable<GetOrderDto>>> GetByStoreId(Guid storeId)
        {
            var operation = new OperationResult<IEnumerable<GetOrderDto>>();

            try
            {
                var orders = await context.Orders
                                          .Where(o => o.ToyOrders.First().Toy.StorSection.StoreId.Equals(storeId))
                                          .Select(order => new GetOrderDto
                                          {
                                              Id = order.Id,
                                              Num = order.Num,
                                              Date = order.Date,
                                              StatusOrder = order.StatusOrder,
                                              UserName = order.User.UserName,
                                              UserId = order.UserId,
                                              TotalPrice = order.TotalPrice,
                                          }).ToListAsync();

                return operation.SetSuccess(orders);
            }
            catch (Exception ex)
            {
                return operation.SetException(ex);
            }
        }


        #region Helper Method
        public static string GenerateUniqueOrderNumber(DbContext dbContext)
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            var random = new Random();
            var result = new StringBuilder(4);
            for (int i = 0; i < 4; i++)
            {
                result.Append(chars[random.Next(chars.Length)]);
            }
            var orderNumber = result.ToString();
            var isUnique = !dbContext.Set<Order>().Any(o => o.Num == orderNumber);
            if (isUnique)
            {
                return orderNumber;
            }
            return GenerateUniqueOrderNumber(dbContext);
        }



        #endregion

    }
}
