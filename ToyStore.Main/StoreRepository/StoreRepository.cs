﻿using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ToyStore.Base;
using ToyStore.DTO.Main.Section;
using ToyStore.DTO.Main.Store;
using ToyStore.DTO.Main.Toys;
using ToyStore.Main.ToysRepository;
using ToyStore.Models.Main;
using ToyStore.Shared.FileRepository;
using ToyStore.SharedKernel.OperationResult;
using ToyStore.SQL.Context;

namespace ToyStore.Main.StoreRepository
{
    public class StoreRepository : BaseReposiotry,IStoreRepository
    {
        private readonly IFileRepository fileRepository;
        private readonly IToysRepository toysRepository;

        public StoreRepository(ToyStoreDBContext context, IFileRepository fileRepository, IToysRepository toysRepository) : base(context)
        {
            this.fileRepository = fileRepository;
            this.toysRepository = toysRepository;
        }

        public async Task<OperationResult<GetStoreDto>> Create(SetStoreDto storeDto)
        {
            var operation = new OperationResult<GetStoreDto>();
            using (var transaction = context.Database.BeginTransaction())
            {
                try
                {
                    var path = await fileRepository.Upload("Store", storeDto.Image);
                    var store = new Store
                    {
                        Name = storeDto.Name,
                        ManagerId = storeDto.ManagerId,
                        ImagPath = path.Result,
                        StorSections = storeDto.SectionIds.Select(sec => new StorSection
                        {
                            SectionId = sec,
                        }).ToList()
                    };
                    context.Add(store);
                    await context.SaveChangesAsync();
                   
                    transaction.Commit();

                    return await GetById(store.Id);
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    return operation.SetException(ex);
                }
            }
        }

        public async Task<OperationResult<bool>> Delete(Guid id)
        {
            var operation = new OperationResult<bool>();
            try
            {
                var store = await context.Stores.Where(store => store.Id.Equals(id))
                                                .Include(store => store.StorSections)
                                                .ThenInclude(store => store.Toys)
                                                .ThenInclude(toy => toy.ToyOrders)
                                                .ThenInclude(o => o.Order)
                                                .SingleOrDefaultAsync();

                if(store.StorSections.Any(ss => ss.Toys
                        .Any(t => t.ToyOrders
                        .Any(o => o.Order.StatusOrder != SharedKernel.Enums.Main.OrderStatus.Received))))
                {
                    return operation.SetFailed("This store has unreceived order", OperationResultType.Failed);
                }

                await fileRepository.Remove(store.ImagPath);
                await context.SaveChangesAsync();

                var sections = store.StorSections.ToList();
                foreach (var sec in sections)
                {
                    foreach (var toy in sec.Toys.ToList())
                    {
                        await toysRepository.Delete(toy.Id);
                    }
                }

                context.Remove(store);
                await context.SaveChangesAsync();

                return operation.SetSuccess(true);
            }
            catch (Exception ex)
            {
                return operation.SetException(ex);
            }
        }

        public async Task<OperationResult<IEnumerable<GetStoreDto>>> GetAll()
        {
            var operation = new OperationResult<IEnumerable<GetStoreDto>>();

            try
            {
                var store = await context.Stores.Select(store => new GetStoreDto
                {
                    Id = store.Id,
                    Name = store.Name,
                    ImagePath = store.ImagPath,
                    ManagrName = store.Manager.FirstName,
                    ManagerId = store.ManagerId,
                }).ToListAsync();
                
                return operation.SetSuccess(store);
            }
            catch (Exception ex)
            {
                return operation.SetException(ex);
            }
        }

        public async Task<OperationResult<GetStoreDto>> GetById(Guid id)
        {
            var operation = new OperationResult<GetStoreDto>();
            try
            {
                var store = await context.Stores.Where(store => store.Id.Equals(id))
                                                .Select(store => new GetStoreDto
                                                {
                                                    Id = store.Id,
                                                    Name = store.Name,
                                                    ImagePath = store.ImagPath,
                                                    ManagrName = store.Manager.FirstName,
                                                    ManagerId = store.ManagerId,
                                                    Sections = store.StorSections.Select(sec => new DetailsSectionDto
                                                    {
                                                        Id = sec.SectionId,
                                                        Name = sec.Section.Name,
                                                        Toys = sec.Toys.Select(toys => new GetToysDto
                                                        {
                                                            Id = toys.Id,
                                                            Name = toys.Name,
                                                            MaxAge = toys.MaxAge,
                                                            MinAge = toys.MinAge,
                                                            Price = toys.Price,
                                                            StorSectionId = sec.Id,
                                                            StoreName= store.Name,
                                                            ImagPath = toys.ImagPath,
                                                            Count = toys.Count,
                                                        }).ToList()
                                                    }).ToList()
                                                }).SingleOrDefaultAsync();

                operation.SetSuccess(store);
            }
            catch (Exception ex)
            {
                operation.SetException(ex);
            }
            return operation;
        }

        public async Task<OperationResult<GetStoreDto>> Update(UpdateStoreDto updateStoreDto)
        {
            var operation = new OperationResult<GetStoreDto>();
            using (var transaction = context.Database.BeginTransaction())
            {
                try
                {
                    var store = await context.Stores
                                               .Where(store => updateStoreDto.Id.Equals(store.Id))
                                               .SingleOrDefaultAsync();

                    if (store is null)
                    {
                        operation.SetFailed($"this store with {store.Id} id not found.");
                        return operation;
                    }

                    store.Name = updateStoreDto.Name;
                    store.ManagerId = updateStoreDto.ManagerId;
                    store.ImagPath = updateStoreDto.ImagePath;

                    context.Update(store);
                    await context.SaveChangesAsync();

                    //update section

                    var storeRes = await GetById(store.Id);
                    operation.SetSuccess(storeRes.Result);

                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    operation.SetException(ex);
                }
            }
            return operation;
        }
    }
}
