﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ToyStore.DTO.Main.Section;
using ToyStore.DTO.Main.Store;
using ToyStore.SharedKernel.OperationResult;

namespace ToyStore.Main.StoreRepository
{
    public interface IStoreRepository
    {
        Task<OperationResult<IEnumerable<GetStoreDto>>> GetAll();
        Task<OperationResult<GetStoreDto>> Create(SetStoreDto storeDto);
        Task<OperationResult<GetStoreDto>> GetById(Guid id);
        Task<OperationResult<GetStoreDto>> Update(UpdateStoreDto storeDto);
        Task<OperationResult<bool>> Delete(Guid id);


    }
}
