﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ToyStore.DTO.Main.City;
using ToyStore.SharedKernel.OperationResult;

namespace ToyStore.Main.CityRepository
{
    public interface ICityRepository
    {
        Task<OperationResult<IEnumerable<GetCityDto>>> GetAll();
    }
}
