﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ToyStore.Base;
using ToyStore.DTO.Main.City;
using ToyStore.DTO.Main.Section;
using ToyStore.Models.Main;
using ToyStore.SharedKernel.OperationResult;
using ToyStore.SQL.Context;

namespace ToyStore.Main.CityRepository
{
    public class CityRepository:BaseReposiotry,ICityRepository
    {
        public CityRepository(ToyStoreDBContext context) : base(context)
        {
        }

        public async Task<OperationResult<IEnumerable<GetCityDto>>> GetAll()
        {
            var operation = new OperationResult<IEnumerable<GetCityDto>>();

            try
            {
                var city = await context.Cities
                                            .Select(city => new GetCityDto
                                            {
                                                Id = city.Id,
                                                Name = city.Name
                                            }).ToListAsync();

                return operation.SetSuccess(city);
            }
            catch (Exception ex)
            {
                return operation.SetException(ex);
            }
        }
    }
}
