﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ToyStore.DTO.Main.Discount;
using ToyStore.DTO.Main.Toys;
using ToyStore.DTO.Main.Section;
using ToyStore.SharedKernel.OperationResult;
using ToyStore.DTO.Main.Store;

namespace ToyStore.Main.ToysRepository
{
    public interface IToysRepository
    {
        Task<OperationResult<GetDiscountDto>> CreateDiscount(SetDiscountDto setDiscountDto);
        Task<OperationResult<bool>> DeleteDiscount(Guid id);

        Task<OperationResult<IEnumerable<GetToysDto>>> GetAll();
        Task<OperationResult<GetToysDto>> GetById(Guid id);
        Task<OperationResult<IEnumerable<GetToysDto>>> GetByStoreId(Guid storeid);
        Task<OperationResult<IEnumerable<GetToysDto>>> GetBySectionId(Guid sectionid);

        Task<OperationResult<GetToysDto>> Update(UpdateToysDto updateToysDto);
        Task<OperationResult<bool>> Delete(Guid id);
        Task<OperationResult<GetToysDto>> Create(SetToysDto setToysDto);


    }
}
