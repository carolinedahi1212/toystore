﻿using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ToyStore.Base;
using ToyStore.DTO.Main.Discount;
using ToyStore.DTO.Main.Section;
using ToyStore.DTO.Main.Store;
using ToyStore.DTO.Main.Toys;
using ToyStore.Models.Main;
using ToyStore.Shared.FileRepository;
using ToyStore.SharedKernel.OperationResult;
using ToyStore.SQL.Context;
using static System.Collections.Specialized.BitVector32;
using static System.Formats.Asn1.AsnWriter;

namespace ToyStore.Main.ToysRepository
{
    public class ToysRepository : BaseReposiotry, IToysRepository
    {
        private readonly IFileRepository fileRepository;

        public ToysRepository(ToyStoreDBContext context, IFileRepository fileRepository) : base(context)
        {
            this.fileRepository = fileRepository;
        }

        public async Task<OperationResult<GetToysDto>> Create(SetToysDto toysDto)
        {
            var operation = new OperationResult<GetToysDto>();
            using (var transaction = context.Database.BeginTransaction())
            {
                try
                {
                    var path = await fileRepository.Upload("Toys", toysDto.Image);
                    var storeSectionId = await context.StorSections.Where(s => s.SectionId.Equals(toysDto.SectionId)
                                                                            && s.StoreId.Equals(toysDto.StorId))
                                                                   .Select(s => s.Id)
                                                                   .FirstOrDefaultAsync();
                    //if(storeSectionId is Guid.Empty)
                    //{
                    //    operation.SetFailed($"this section with {toysDto.SectionId} id not found or this store wiht {toysDto.StorId} id not found.");
                    //    return operation;
                    //}
                    var images = await fileRepository.Add("Toys", toysDto.Images);
                    var toy = new Toy
                    {
                        Name = toysDto.Name,
                        Price= toysDto.Price,
                        Count= toysDto.Coount,
                        ImagPath = path.Result,
                        MaxAge = toysDto.MaxAge,
                        MinAge = toysDto.MinAge,
                        StorSectionId = storeSectionId,
                        ToyFiles = images.Result.ToList().Select(i => new ToyFile
                        {
                            FileId = i.Id,
                        }).ToList(),
                    };
                    context.Add(toy);
                    await context.SaveChangesAsync();

                    transaction.Commit();

                    return await GetById(toy.Id);
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    return operation.SetException(ex);
                }
            }
        }

        public async Task<OperationResult<GetDiscountDto>> CreateDiscount(SetDiscountDto setDiscountDto)
        {
            var operation = new OperationResult<GetDiscountDto>();
            using (var transaction = context.Database.BeginTransaction())
            {
                try
                {
                    var dis = new Discount
                    {
                        StartDate = setDiscountDto.StartDate,
                        EndDate = setDiscountDto.EndDate,
                        Percentage = setDiscountDto.Percentage,
                        ToyId = setDiscountDto.ToyId
                    };
                    context.Add(dis);
                    await context.SaveChangesAsync();
                    transaction.Commit();

                    return operation;
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    return operation.SetException(ex);
                }
            }
        }

        public async Task<OperationResult<bool>> Delete(Guid id)
        {
            var operation = new OperationResult<bool>();
            try
            {
                var toy = await context.Toys.Where(toy => toy.Id.Equals(id))
                                            .Include(toy => toy.ToyFiles)
                                            .Include(toy => toy.ToyOrders)
                                            .ThenInclude(o => o.Order)
                .SingleOrDefaultAsync();

                if (toy.ToyOrders
                        .Any(o => o.Order.StatusOrder != SharedKernel.Enums.Main.OrderStatus.Received))
                {
                    return operation.SetFailed("This toy has unreceived order", OperationResultType.Failed);
                }
                await fileRepository.Remove(toy.ImagPath);
                await fileRepository.Delete(toy.ToyFiles.Select(tf => tf.FileId).ToList());
                context.Remove(toy.ToyOrders.Select(to => to.Order).ToList());
                context.Remove(toy.ToyOrders);
                context.RemoveRange(toy.ToyFiles);
                context.Remove(toy);
                await context.SaveChangesAsync();

                return operation.SetSuccess(true);
            }
            catch (Exception ex)
            {
                return operation.SetException(ex);
            }
        }

        public async Task<OperationResult<bool>> DeleteDiscount(Guid id)
        {
            var operation = new OperationResult<bool>();
            try
            {
                var dis = await context.Discounts.Where(d => d.Id.Equals(id)).SingleOrDefaultAsync();
                context.Remove(dis);
                await context.SaveChangesAsync();

                return operation.SetSuccess(true);
            }
            catch (Exception ex)
            {
                return operation.SetException(ex);
            }
        }

        public async Task<OperationResult<IEnumerable<GetToysDto>>> GetAll()
        {
            var operation = new OperationResult<IEnumerable<GetToysDto>>();

            try
            {
                var toy = await context.Toys
                                       .Select(toy => new GetToysDto
                                       {
                                           Id = toy.Id,
                                           Name = toy.Name,
                                           Price=toy.Price,
                                           Count=toy.Count,
                                           ImagPath=toy.ImagPath,
                                           MaxAge=toy.MaxAge,
                                           MinAge=toy.MinAge,
                                           Discount = toy.Discounts.Where(c => c.StartDate < DateTime.Now 
                                                                            && c.EndDate >= DateTime.Now)
                                                                   .FirstOrDefault().Percentage,
                                           StoreName=toy.StorSection.Store.Name,
                                           Evaluation = toy.ToyOrders.Any(to => to.Evaluation != 0) ?
                                                        toy.ToyOrders.Sum(to => to.Evaluation) / toy.ToyOrders.Count(to => to.Evaluation != 0) : 0,
                                       }).ToListAsync();

                return operation.SetSuccess(toy);
            }
            catch (Exception ex)
            {
                return operation.SetException(ex);
            }
        }

        public async Task<OperationResult<GetToysDto>> GetById(Guid id)
        {
            var operation = new OperationResult<GetToysDto>();
            try
            {
                var toy = await context.Toys.Where(toy => toy.Id.Equals(id))
                                            .Select(toy => new GetToysDto
                                            {
                                                Id = toy.Id,
                                                Name = toy.Name,
                                                Price = toy.Price,
                                                Count = toy.Count,
                                                ImagPath = toy.ImagPath,
                                                MaxAge = toy.MaxAge,
                                                MinAge = toy.MinAge,
                                                StoreName = toy.StorSection.Store.Name,
                                                StoreId=toy.StorSection.StoreId,
                                                SectionId=toy.StorSection.SectionId,
                                                Discount=toy.Discounts.Where(c=>c.StartDate <= DateTime.Now 
                                                                             && c.EndDate >= DateTime.Now)
                                                                            .FirstOrDefault().Percentage,
                                                Evaluation = toy.ToyOrders.Any(to => to.Evaluation != 0) ?
                                                        toy.ToyOrders.Sum(to => to.Evaluation) / toy.ToyOrders.Count(to => to.Evaluation != 0) : 0,

                                            }).SingleOrDefaultAsync();

                operation.SetSuccess(toy);
            }
            catch (Exception ex)
            {
                operation.SetException(ex);
            }
            return operation;
        }

        public async Task<OperationResult<IEnumerable<GetToysDto>>> GetBySectionId(Guid sectionid)
        {
            var operation = new OperationResult<IEnumerable<GetToysDto>>();
            try
            {
                var toys = await context.Toys.Where(sec => sec.StorSection.SectionId.Equals(sectionid))
                                             .Select(toy => new GetToysDto
                                             {
                                                 Id = toy.Id,
                                                 Name = toy.Name,
                                                 Price = toy.Price,
                                                 Count = toy.Count,
                                                 ImagPath = toy.ImagPath,
                                                 MaxAge = toy.MaxAge,
                                                 MinAge = toy.MinAge,
                                                 StoreName = toy.StorSection.Store.Name,
                                                 StorSectionId = toy.StorSectionId,
                                                 Evaluation = toy.ToyOrders.Any(to => to.Evaluation != 0) ?
                                                        toy.ToyOrders.Sum(to => to.Evaluation) / toy.ToyOrders.Count(to => to.Evaluation != 0) : 0,

                                             }).ToListAsync();

                return operation.SetSuccess(toys);
            }
            catch (Exception ex)
            {
                return operation.SetException(ex);
            }
        }

        public async Task<OperationResult<IEnumerable<GetToysDto>>> GetByStoreId(Guid storeid)
        {
            var operation = new OperationResult<IEnumerable<GetToysDto>>();
            try
            {
                var toys = await context.Toys.Where(sec => sec.StorSection.StoreId.Equals(storeid))
                                             .Select(toy => new GetToysDto
                                             {
                                                 Id = toy.Id,
                                                 Name = toy.Name,
                                                 Price = toy.Price,
                                                 Count = toy.Count,
                                                 ImagPath = toy.ImagPath,
                                                 MaxAge = toy.MaxAge,
                                                 MinAge = toy.MinAge,
                                                 StoreName = toy.StorSection.Store.Name,
                                                 StorSectionId = toy.StorSectionId,
                                                 Evaluation = toy.ToyOrders.Any(to => to.Evaluation != 0) ?
                                                        toy.ToyOrders.Sum(to => to.Evaluation) / toy.ToyOrders.Count(to => to.Evaluation != 0) : 0,

                                             }).ToListAsync();

                return operation.SetSuccess(toys);
            }
            catch (Exception ex)
            {
                return operation.SetException(ex);
            }
        }

        public async Task<OperationResult<GetToysDto>> Update(UpdateToysDto updateToysDto)
        {
            var operation = new OperationResult<GetToysDto>();
            using (var transaction = context.Database.BeginTransaction())
            {
                try
                {
                    var toy = await context.Toys
                                               .Where(toy => updateToysDto.Id.Equals(toy.Id))
                                               .SingleOrDefaultAsync();

                    if (toy is null)
                    {
                        operation.SetFailed($"this toy with {toy.Id} id not found.");
                        return operation;
                    }

                    toy.Name = updateToysDto.Name;
                    toy.ImagPath = updateToysDto.ImagPath;
                    toy.MaxAge = updateToysDto.MaxAge;
                    toy.MinAge = updateToysDto.MinAge;
                    toy.Price = updateToysDto.Price;
                    toy.Count = updateToysDto.Coount;
                    toy.StorSectionId = updateToysDto.StorSectionId;

                    context.Update(toy);
                    await context.SaveChangesAsync();

                    var toyRes = await GetById(toy.Id);
                    operation.SetSuccess(toyRes.Result);

                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    operation.SetException(ex);
                }
            }
            return operation;
        }
    }
}
