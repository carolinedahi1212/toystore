﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ToyStore.DTO.Main.CommonQuestion;
using ToyStore.DTO.Main.Section;
using ToyStore.SharedKernel.OperationResult;

namespace ToyStore.Main.CommonQuestionRepository
{
    public interface ICommonQuestionRepository
    {
        Task<OperationResult<IEnumerable<GetCommonQuestionDto>>> GetAll();
        Task<OperationResult<GetCommonQuestionDto>> GetById(Guid id);
        Task<OperationResult<GetCommonQuestionDto>> Create(SetCommonQuestionDto setCommonQuestionDto);
        Task<OperationResult<GetCommonQuestionDto>> Update(UpdateCommonQuestionDto updateCommonQuestionDto);
        Task<OperationResult<bool>> Delete(Guid id);
    }
}
