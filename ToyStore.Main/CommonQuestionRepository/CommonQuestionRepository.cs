﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ToyStore.Base;
using ToyStore.DTO.Main.CommonQuestion;
using ToyStore.DTO.Main.Section;
using ToyStore.Main.SectionRepository;
using ToyStore.Models.Main;
using ToyStore.SharedKernel.OperationResult;
using ToyStore.SQL.Context;

namespace ToyStore.Main.CommonQuestionRepository
{
    public class CommonQuestionRepository : BaseReposiotry, ICommonQuestionRepository
    {

        public CommonQuestionRepository(ToyStoreDBContext context) : base(context)
        {
        }


        public async Task<OperationResult<GetCommonQuestionDto>> Create(SetCommonQuestionDto setCommonQuestionDto)
        {
            var operation = new OperationResult<GetCommonQuestionDto>();
            using (var transaction = context.Database.BeginTransaction())
            {
                try
                {
                    var common = new CommonQuestion
                    {
                        Question = setCommonQuestionDto.Question,
                        Answer=setCommonQuestionDto.Answer
                    };
                    context.Add(common);
                    await context.SaveChangesAsync();
                    transaction.Commit();

                    return await GetById(common.Id);
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    return operation.SetException(ex);
                }
            }
        }

        public async Task<OperationResult<bool>> Delete(Guid id)
        {
                var operation = new OperationResult<bool>();
                try
                {
                    var common = await context.CommonQuestions
                                              .Where(com => com.Id.Equals(id))
                                              .SingleOrDefaultAsync();
                    context.Remove(common);
                    await context.SaveChangesAsync();
                    return operation.SetSuccess(true);
                }
                catch (Exception ex)
                {
                    return operation.SetException(ex);
                }
        }

        public async Task<OperationResult<IEnumerable<GetCommonQuestionDto>>> GetAll()
        {
            var operation = new OperationResult<IEnumerable<GetCommonQuestionDto>>();

            try
            {
                var common = await context.CommonQuestions
                                          .Select(com => new GetCommonQuestionDto
                                          {
                                             Id = com.Id,
                                             Question = com.Question,
                                             Answer=com.Answer
                                          }).ToListAsync();

                return operation.SetSuccess(common);
            }
            catch (Exception ex)
            {
                return operation.SetException(ex);
            }
        }

        public async Task<OperationResult<GetCommonQuestionDto>> GetById(Guid id)
        {
            var operation = new OperationResult<GetCommonQuestionDto>();
            try
            {
                var common = await context.CommonQuestions
                                          .Where(com => com.Id.Equals(id))
                                          .Select(com => new GetCommonQuestionDto
                                          {
                                              Id=com.Id,
                                              Question=com.Question,
                                              Answer=com.Answer
                                          }).SingleOrDefaultAsync();

                operation.SetSuccess(common);
            }
            catch (Exception ex)
            {
                operation.SetException(ex);
            }
            return operation;
        }

        public async Task<OperationResult<GetCommonQuestionDto>> Update(UpdateCommonQuestionDto updateCommonQuestionDto)
        {
            var operation = new OperationResult<GetCommonQuestionDto>();
            using (var transaction = context.Database.BeginTransaction())
            {
                try
                {
                    var common = await context.CommonQuestions
                                               .Where(com => updateCommonQuestionDto.Id.Equals(com.Id))
                                               .SingleOrDefaultAsync();

                    if (common is null)
                    {
                        operation.SetFailed($"this commonQuestion with {common.Id} id not found.");
                        return operation;
                    }

                    common.Question = updateCommonQuestionDto.Question;
                    common.Answer = updateCommonQuestionDto.Answer;

                    context.Update(common);
                    await context.SaveChangesAsync();

                    var commonRes = await GetById(common.Id);
                    operation.SetSuccess(commonRes.Result);

                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    operation.SetException(ex);
                }
            }
            return operation;
        }
    }
}
