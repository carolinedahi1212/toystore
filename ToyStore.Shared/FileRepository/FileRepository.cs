﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.JsonPatch.Operations;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Hosting.Internal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ToyStore.Base;
using ToyStore.DTO.Shared;
using ToyStore.Models.Shared;
using ToyStore.SharedKernel.ExtensionMethods;
using ToyStore.SharedKernel.OperationResult;
using ToyStore.SQL.Context;

namespace ToyStore.Shared.FileRepository
{
    public class FileRepository : BaseReposiotry, IFileRepository
    {
        private readonly IHostingEnvironment hostingEnvironment;

        public FileRepository(ToyStoreDBContext context, IHostingEnvironment hostingEnvironment) : base(context)
        {
            this.hostingEnvironment = hostingEnvironment;
        }

        public async Task<OperationResult<List<GetFileDto>>> Add(string foldername, List<IFormFile> files)
        {
            var operation = new OperationResult<List<GetFileDto>>();
            try
            {
                TryUploadFile(foldername, files, out List<string> paths, out List<string> names);
                List<_File> FilesToAdd = new List<_File>();
                foreach (var path in paths)
                {
                    var file = new _File()
                    {
                        Name = names[paths.IndexOf(path)],
                        Path = path,
                        Type = ExtensionMethods.TypeOfFile(files.First()),
                    };
                    FilesToAdd.Add(file);
                }
                context.AddRange(FilesToAdd);
                await context.SaveChangesAsync();

                return operation.SetSuccess(FilesToAdd.Select(d => new GetFileDto
                {
                    Id = d.Id,
                    Name = d.Name,
                    Path = d.Path,
                    Type = d.Type,
                }).ToList());
            }
            catch (Exception ex)
            {
                return operation.SetException(ex);
            }
        }

        public async Task<OperationResult<bool>> Delete(List<Guid> ids)
        {
            var operation = new OperationResult<bool>();
            using (var transaction = context.Database.BeginTransaction())
            {
                try
                {
                    var files = await context.Files.Where(d => ids.Contains(d.Id)).ToListAsync();
                    TryDeleteFile(files.Select(d => d.Path).ToList());
                    context.RemoveRange(files);
                    await context.SaveChangesAsync();
                    operation.SetSuccess(true);
                }
                catch (Exception ex)
                {
                    operation.SetException(ex);
                }
            }
            return operation;
        }

        public async Task<OperationResult<string>> Upload(string foldername, IFormFile file)
        {
            var operation = new OperationResult<string>();
            try
            {
                if (file == null)
                {
                    operation.SetSuccess("");
                }

                TryUploadFile(foldername, new List<IFormFile>() { file }, out List<string> paths, out List<string> names);

                operation.SetSuccess(paths.First());
            }
            catch (Exception ex)
            {
                operation.SetException(ex);
            }
            return operation;
        }

        public async Task<OperationResult<bool>> Remove(string path)
        {
            var operation = new OperationResult<bool>();
            TryDeleteFile(new List<string>() { path });

            return operation.SetSuccess(true);
        }


        #region Helper Methods
        private bool TryUploadFile(string foldername, List<IFormFile> files, out List<string> paths, out List<string> names)
        {
            paths = new List<string>();
            names = new List<string>();
            try
            {
                if (files != null)
                {
                    var filesDirectory = Path.Combine("wwwroot", "Files", foldername);

                    if (!Directory.Exists(filesDirectory))
                    {
                        Directory.CreateDirectory(filesDirectory);
                    }
                    foreach (var file in files)
                    {
                        var path = Path.Combine("Files", foldername, Guid.NewGuid().ToString() + "_" + file.FileName);
                        paths.Add(path);
                        names.Add(file.FileName);
                        string fileName = Path.Combine(hostingEnvironment.WebRootPath, path);
                        using (var fileStream = new FileStream(fileName, FileMode.Create))
                        {
                            file.CopyTo(fileStream);
                        }
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        private async Task<bool> TryDeleteFile(List<string> paths)
        {
            if (paths != null)
            {
                try
                {
                    foreach (var path in paths)
                    {
                        var pathFile = Path.Combine(hostingEnvironment.WebRootPath, path);
                        File.Delete(pathFile);
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            return true;
        }
        #endregion

    }
}
