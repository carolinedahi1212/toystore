﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ToyStore.Models.Base;

namespace ToyStore.Models.Main
{
    public class StorSection : BaseEntity
    {
        public Guid StoreId { get; set; }
        public Store Store { get; set; }

        public Guid SectionId { get; set; }
        public Section Section { get; set; }

        public ICollection<Toy> Toys { get; set; }
    }
}
