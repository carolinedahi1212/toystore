﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ToyStore.Models.Base;
using ToyStore.Models.Security;

namespace ToyStore.Models.Main
{
    public class ContactUs : BaseEntity
    {
        public string Title { get; set; }
        public string Description { get; set; }
        public string? Reply { get; set; }
        public DateTime? ReplyDate { get; set; }

        public Guid SenderId { get; set; }
        public User Sender { get; set; }

        public Guid? ReplaierId { get; set; }
        public User? Replaier { get; set; }

        public Guid? OrderId { get; set; }
        public Order Order { get; set; }
    }
}
