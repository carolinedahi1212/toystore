﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ToyStore.Models.Base;
using ToyStore.Models.Shared;

namespace ToyStore.Models.Main
{
    public class ToyFile : BaseEntity
    {
        public Guid FileId { get; set; }
        public _File File { get; set; }

        public Guid ToyId { get; set; }
        public Toy Toy { get; set; }
    }
}
