﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ToyStore.Models.Base;
using ToyStore.Models.Security;

namespace ToyStore.Models.Main
{
    public class Store : BaseEntity
    {
        public string Name { get; set; }
        public string ImagPath { get; set; }

        public Guid ManagerId { get; set; }
        public User Manager { get; set; }

        public ICollection<StorSection> StorSections { get; set; }
    }
}
