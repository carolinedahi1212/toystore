﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ToyStore.Models.Base;

namespace ToyStore.Models.Main
{
    public class CommonQuestion : BaseEntity
    {
        public string Question { get; set; }
        public string Answer { get; set; }
    }
}
