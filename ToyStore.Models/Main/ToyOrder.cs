﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ToyStore.Models.Base;

namespace ToyStore.Models.Main
{
    public class ToyOrder : BaseEntity
    {
        public Guid OrderId { get; set; }
        public Order Order { get; set; }

        public Guid ToyId { get; set; }
        public Toy Toy { get; set; }

        public double FinalPrice { get; set; }

        public int ToyCount { get; set; }
        public int Evaluation { get; set; }

    }
}
