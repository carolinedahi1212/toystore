﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ToyStore.Models.Base;

namespace ToyStore.Models.Main
{
    public class Toy : BaseEntity
    {
        public string Name { get; set; }
        public double Price { get; set; }
        public int Count { get; set; }
        public string ImagPath { get; set; }
        public int MinAge { get; set; }
        public int MaxAge { get; set; }
        public string Description { get; set; }

        public Guid StorSectionId { get; set; }
        public StorSection StorSection { get; set; }

        public ICollection<Discount> Discounts { get; set; }

        public ICollection<ToyOrder> ToyOrders { get; set; }

        public ICollection<ToyFile> ToyFiles { get; set; }

    }
}
