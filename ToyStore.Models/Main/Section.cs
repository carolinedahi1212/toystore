﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ToyStore.Models.Base;

namespace ToyStore.Models.Main
{
    public class Section : BaseEntity
    {
        public string Name { get; set; }

        public ICollection<StorSection> StorSections { get; set; }
    }
}
