﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ToyStore.Models.Base;
using ToyStore.Models.Security;
using ToyStore.SharedKernel.Enums.Main;

namespace ToyStore.Models.Main
{
    public class Order : BaseEntity
    {
        public string Num { get; set; }
        public DateTime Date { get; set; }
        public OrderStatus StatusOrder { get; set; }
        public double TotalPrice { get; set; }

        public Guid UserId { get; set; }
        public User User { get; set; }

        public Guid? ComplaintId { get; set; }
        public ContactUs Complaint { get; set; }


        public ICollection<ToyOrder> ToyOrders { get; set; }
    }
}
