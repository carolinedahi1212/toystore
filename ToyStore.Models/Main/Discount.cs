﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ToyStore.Models.Base;

namespace ToyStore.Models.Main
{
    public class Discount : BaseEntity
    {
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public double Percentage { get; set; }

        public Toy Toy { get; set; }
        public Guid ToyId { get; set; }
    }
}
