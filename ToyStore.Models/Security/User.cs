﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ToyStore.Models.Main;
using ToyStore.SharedKernel.Enums.Security;

namespace ToyStore.Models.Security
{
    public class User : IdentityUser<Guid>
    {
        public string? GenerationStamp { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string? Address { get; set; }
        public UserType UserType { get; set; }
        public string? ImagePath { get; set; }

        public Guid CityId { get; set; }
        public City City { get; set; }

        public ICollection<ContactUs> ContactUsSends { get; set; }
        public ICollection<ContactUs> ContactUsReplaies { get; set; }
        public ICollection<Order> Orders { get; set; }
    }
}
