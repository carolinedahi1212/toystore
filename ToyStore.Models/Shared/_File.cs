﻿using Microsoft.VisualBasic.FileIO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ToyStore.Models.Main;
using ToyStore.Models.Security;
using ToyStore.SharedKernel.Enums.Shared;

namespace ToyStore.Models.Shared
{
    public class _File
    {
        public Guid Id { get; set; }
        public string Path { get; set; }
        public string Name { get; set; }
        public FileType Type { get; set; }

        public Guid? StoreId { get; set; }
        public Store Store { get; set; }

        public Guid? UserId { get; set; }
        public User User { get; set; }

        public ICollection<ToyFile> ToyFiles { get; set; }
    }
}
