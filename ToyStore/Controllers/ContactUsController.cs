﻿using Microsoft.AspNetCore.Mvc;
using System.ComponentModel.DataAnnotations;
using ToyStore.DTO.Main.ContactUs;
using ToyStore.Main.ContactUsRepository;
using ToyStore.SharedKernel.ExtensionMethods;
using ToyStore.SharedKernel.OperationResult;

namespace ToyStore.Controllers
{
    [ApiController]
    [Route("api/[controller]/[action]")]
    public class ContactUsController : ControllerBase
    {
        private readonly IContactUsRepository contactUsRepository;

        public ContactUsController(IContactUsRepository contactUsRepository)
        {
            this.contactUsRepository = contactUsRepository;
        }

        [HttpGet]
        public async Task<IActionResult> GetAll()
           => await contactUsRepository.GetAll().ToJsonResultAsync();

        [HttpGet]
        public async Task<IActionResult> GetById([Required] Guid id)
           => await contactUsRepository.GetById(id).ToJsonResultAsync();

        [HttpPost]
        public async Task<IActionResult> Create(SetContactDto contactUsDto)
           => await contactUsRepository.Create(contactUsDto).ToJsonResultAsync();

        [HttpDelete]
        public async Task<IActionResult> Delete([Required] Guid id)
           => await contactUsRepository.Delete(id).ToJsonResultAsync();

        [HttpPost]
        public async Task<IActionResult> Response(ResponseContactDto contactUsDto)
           => await contactUsRepository.Response(contactUsDto).ToJsonResultAsync();
    }
}
