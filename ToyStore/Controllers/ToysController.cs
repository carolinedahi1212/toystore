﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.ComponentModel.DataAnnotations;
using ToyStore.DTO.Main.Discount;
using ToyStore.DTO.Main.Store;
using ToyStore.DTO.Main.Toys;
using ToyStore.Main.SectionRepository;
using ToyStore.Main.StoreRepository;
using ToyStore.Main.ToysRepository;
using ToyStore.SharedKernel.OperationResult;

namespace ToyStore.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class ToysController : ControllerBase
    {
        private readonly IToysRepository toysRepository;

        public ToysController(IToysRepository toysRepository)
        {
            this.toysRepository = toysRepository;
        }

        [HttpPost]
        public async Task<IActionResult> CreateDiscount(SetDiscountDto setDiscountDto)
            => await toysRepository.CreateDiscount(setDiscountDto).ToJsonResultAsync();

        [HttpDelete]
        public async Task<IActionResult> DeleteDiscount([Required] Guid id)
            => await toysRepository.DeleteDiscount(id).ToJsonResultAsync();

        [HttpGet]
        public async Task<IActionResult> GetToys()
            => await toysRepository.GetAll().ToJsonResultAsync();

        [HttpGet]
        public async Task<IActionResult> GetById([Required] Guid id)
            => await toysRepository.GetById(id).ToJsonResultAsync();

        [HttpGet]
        public async Task<IActionResult> GetByStoreId([Required] Guid id)
              => await toysRepository.GetByStoreId(id).ToJsonResultAsync();

        [HttpGet]
        public async Task<IActionResult> GetBySectionId([Required] Guid id)
              => await toysRepository.GetBySectionId(id).ToJsonResultAsync();

        [HttpPost]
        public async Task<IActionResult> Create([FromForm] SetToysDto toysDto)
            => await toysRepository.Create(toysDto).ToJsonResultAsync();

        [HttpDelete]
        public async Task<IActionResult> Delete([Required] Guid id)
            => await toysRepository.Delete(id).ToJsonResultAsync();
    }
}
