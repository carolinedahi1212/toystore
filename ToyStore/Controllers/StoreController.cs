﻿using Microsoft.AspNetCore.Mvc;
using System.ComponentModel.DataAnnotations;
using ToyStore.DTO.Main.Section;
using ToyStore.DTO.Main.Store;
using ToyStore.Main.SectionRepository;
using ToyStore.Main.StoreRepository;
using ToyStore.SharedKernel.OperationResult;

namespace ToyStore.Controllers
{
    [ApiController]
    [Route("api/[controller]/[action]")]
    public class StoreController : ControllerBase
    {
        private readonly IStoreRepository storeRepository;

        public StoreController(IStoreRepository storeRepository)
        {
            this.storeRepository = storeRepository;
        }

        [HttpGet]
        public async Task<IActionResult> GetStores()
            => await storeRepository.GetAll().ToJsonResultAsync();

        [HttpGet]
        public async Task<IActionResult> GetById([Required] Guid id)
            => await storeRepository.GetById(id).ToJsonResultAsync();

        [HttpPost]
        public async Task<IActionResult> Create([FromForm] SetStoreDto storeDto)
            => await storeRepository.Create(storeDto).ToJsonResultAsync();

        [HttpDelete]
        public async Task<IActionResult> Delete([Required] Guid id)
            => await storeRepository.Delete(id).ToJsonResultAsync();
    }
}
