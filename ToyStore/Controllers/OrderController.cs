﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.ComponentModel.DataAnnotations;
using ToyStore.DTO.Main.Order;
using ToyStore.DTO.Main.Section;
using ToyStore.Main.OrderRepository;
using ToyStore.Main.SectionRepository;
using ToyStore.SharedKernel.OperationResult;

namespace ToyStore.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class OrderController : ControllerBase
    {
        private readonly IOrderRepository orderRepository;

        public OrderController(IOrderRepository orderRepository)
        {
            this.orderRepository = orderRepository;
        }

        [HttpGet]
        public async Task<IActionResult> GetOrders()
            => await orderRepository.GetAll().ToJsonResultAsync();

        [HttpGet]
        public async Task<IActionResult> GetById([Required] Guid id)
            => await orderRepository.GetById(id).ToJsonResultAsync();

        [HttpGet]
        public async Task<IActionResult> GetByStoreId([Required] Guid id)
            => await orderRepository.GetByStoreId(id).ToJsonResultAsync();

        [HttpPost]
        public async Task<IActionResult> Create(SetOrderDto orderDto)
            => await orderRepository.Create(orderDto).ToJsonResultAsync();

        [HttpPost]
        public async Task<IActionResult> OnWayOrder([Required] Guid id)
            => await orderRepository.OnWayOrder(id).ToJsonResultAsync();

        [HttpPost]
        public async Task<IActionResult> FinishOrder([Required] Guid id)
            => await orderRepository.FinishOrder(id).ToJsonResultAsync();

        [HttpPost]
        public async Task<IActionResult> EvaluateOrder(EvaluateOrderDto evaluateDto)
            => await orderRepository.EvaluateOrder(evaluateDto).ToJsonResultAsync();
    }
}
