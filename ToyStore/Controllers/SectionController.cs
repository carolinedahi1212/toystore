﻿using Microsoft.AspNetCore.Mvc;
using System.ComponentModel.DataAnnotations;
using ToyStore.DTO.Main.Section;
using ToyStore.Main.SectionRepository;
using ToyStore.SharedKernel.OperationResult;

namespace ToyStore.Controllers
{
    [ApiController]
    [Route("api/[controller]/[action]")]
    public class SectionController : ControllerBase
    {
        private readonly ISectionRepository sectionRepository;

        public SectionController(ISectionRepository sectionRepository)
        {
            this.sectionRepository = sectionRepository;
        }

        [HttpGet]
        public async Task<IActionResult> GetSections()
            => await sectionRepository.GetAll().ToJsonResultAsync();

        [HttpGet]
        public async Task<IActionResult> GetById([Required] Guid id)
            => await sectionRepository.GetById(id).ToJsonResultAsync();

        [HttpPost]
        public async Task<IActionResult> Create(SetSectionDto sectionDto)
            => await sectionRepository.Create(sectionDto).ToJsonResultAsync();

        [HttpPut]
        public async Task<IActionResult> Update(UpdateSectionDto sectionDto)
            => await sectionRepository.Update(sectionDto).ToJsonResultAsync();

        [HttpDelete]
        public async Task<IActionResult> Delete([Required] Guid id)
            => await sectionRepository.Delete(id).ToJsonResultAsync();

        [HttpGet]
        public async Task<IActionResult> GetByStoreId([Required] Guid id)
            => await sectionRepository.GetByStoreId(id).ToJsonResultAsync();
    }
}
