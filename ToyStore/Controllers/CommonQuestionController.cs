﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.ComponentModel.DataAnnotations;
using ToyStore.DTO.Main.CommonQuestion;
using ToyStore.DTO.Main.Section;
using ToyStore.Main.CommonQuestionRepository;
using ToyStore.Main.SectionRepository;
using ToyStore.SharedKernel.OperationResult;

namespace ToyStore.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class CommonQuestionController : ControllerBase
    {
        private readonly ICommonQuestionRepository commonQuestionRepository;

        public CommonQuestionController(ICommonQuestionRepository commonQuestionRepository)
        {
            this.commonQuestionRepository = commonQuestionRepository;
        }

        [HttpGet]
        public async Task<IActionResult> GetCommonQuestion()
            => await commonQuestionRepository.GetAll().ToJsonResultAsync();

        [HttpGet]
        public async Task<IActionResult> GetById([Required] Guid id)
            => await commonQuestionRepository.GetById(id).ToJsonResultAsync();

        [HttpPost]
        public async Task<IActionResult> Create(SetCommonQuestionDto commonQuestionDto)
            => await commonQuestionRepository.Create(commonQuestionDto).ToJsonResultAsync();

        [HttpPut]
        public async Task<IActionResult> Update(UpdateCommonQuestionDto commonQuestionDto)
            => await commonQuestionRepository.Update(commonQuestionDto).ToJsonResultAsync();

        [HttpDelete]
        public async Task<IActionResult> Delete([Required] Guid id)
            => await commonQuestionRepository.Delete(id).ToJsonResultAsync();
    }
}
