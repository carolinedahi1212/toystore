﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using ToyStore.DTO.Security.User;
using ToyStore.Main.SectionRepository;
using ToyStore.Security.UserRepository;
using ToyStore.SharedKernel.ExtensionMethods;
using ToyStore.SharedKernel.OperationResult;

namespace ToyStore.Controllers
{
    [ApiController]
    [Route("api/[controller]/[action]")]
    public class UserController : ControllerBase
    {
        private readonly IUserRepository userRepository;

        public UserController(IUserRepository userRepository)
        {
            this.userRepository = userRepository;
        }

        [HttpPost]
        public async Task<IActionResult> Login(LoginDto loginDto)
            => await userRepository.Login(loginDto).ToJsonResultAsync();

        [HttpPost]
        [AllowAnonymous]
        public async Task<IActionResult> RefreshToken(Guid id, string refreshToken)
        => await userRepository.RefreshToken(id, refreshToken).ToJsonResultAsync();

        [HttpPost]
        public async Task<IActionResult> Create(SetUserDto userDto)
        => await userRepository.Create(userDto).ToJsonResultAsync();

        [HttpGet]
        public async Task<IActionResult> GetAll()
            => await userRepository.GetAll().ToJsonResultAsync();

    }
}
