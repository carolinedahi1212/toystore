﻿using Microsoft.AspNetCore.Mvc;
using System.ComponentModel.DataAnnotations;
using ToyStore.DTO.Main.City;
using ToyStore.Main.CityRepository;
using ToyStore.Main.SectionRepository;
using ToyStore.SharedKernel.OperationResult;

namespace ToyStore.Controllers
{
    [ApiController]
    [Route("api/[controller]/[action]")]

    public class CityController: ControllerBase
    {
        private readonly ICityRepository CityRepository;
        public CityController(ICityRepository cityRepository)
        {
            this.CityRepository = cityRepository;
        }

        [HttpGet]
        public async Task<IActionResult> GetCity()
            => await CityRepository.GetAll().ToJsonResultAsync();

    }
}
