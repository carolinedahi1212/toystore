﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ToyStore.SharedKernel.Enums.Shared;

namespace ToyStore.SharedKernel.ExtensionMethods
{
    public static class ExtensionMethods
    {
        public static FileType TypeOfFile(IFormFile file)
        {
            var type = new FileType();
            if (file.ContentType == "image/gif")
            {
                type = FileType.Image;
            }
            else if (file.ContentType == "application/pdf")
            {
                type = FileType.Document;
            }
            else
            {
                type = FileType.File;
            }
            return type;
        }
    }
}
