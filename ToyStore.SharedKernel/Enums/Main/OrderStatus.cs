﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ToyStore.SharedKernel.Enums.Main
{
    public enum OrderStatus
    {
        InProcess = 1,
        OnWay = 2,
        Received = 3,
    }
}
