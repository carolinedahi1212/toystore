﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ToyStore.SharedKernel.Enums.Shared
{
    public enum FileType
    {
        Image = 1,
        File = 2,
        Document = 3,
    }
}
