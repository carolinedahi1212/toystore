﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ToyStore.DTO.Security.User;
using ToyStore.SharedKernel.OperationResult;

namespace ToyStore.Security.UserRepository
{
    public interface IUserRepository
    {
        Task<OperationResult<GetUserDto>> Login(LoginDto loginDto);
        Task<OperationResult<TokenDto>> RefreshToken(Guid id, string refreshToken);
        Task<OperationResult<GetUserDto>> Create(SetUserDto userDto);
        Task<OperationResult<IEnumerable<GetUserDto>>> GetAll();
    }
}
