﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.JsonPatch.Operations;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using ToyStore.Base;
using ToyStore.DTO.Main.Section;
using ToyStore.DTO.Security.User;
using ToyStore.Models.Main;
using ToyStore.Models.Security;
using ToyStore.Shared.FileRepository;
using ToyStore.SharedKernel.Enums.Security;
using ToyStore.SharedKernel.ExtensionMethods;
using ToyStore.SharedKernel.OperationResult;
using ToyStore.SQL.Context;

namespace ToyStore.Security.UserRepository
{
    public class UserRepository : BaseReposiotry, IUserRepository
    {
        private readonly UserManager<User> userManager;
        private readonly SignInManager<User> signInManager;
        private readonly IConfiguration configuration;
        private readonly IFileRepository fileRepository;

        public UserRepository(ToyStoreDBContext context, UserManager<User> userManager, IFileRepository fileRepository,
                                    SignInManager<User> signInManager, IConfiguration configuration) : base(context)
        {
            this.userManager = userManager;
            this.signInManager = signInManager;
            this.configuration = configuration;
            this.fileRepository = fileRepository;
        }

        public async Task<OperationResult<GetUserDto>> Create(SetUserDto userDto)
        {
            var operation = new OperationResult<GetUserDto>();
            try
            {
                var path = await fileRepository.Upload("User", userDto.Image);
                var user = new User
                {
                    FirstName = userDto.FirstName,
                    LastName = userDto.LastName,
                    UserName = userDto.UserName,
                    Email = userDto.Email,
                    PhoneNumber = userDto.PhoneNumber,
                    UserType = userDto.UserType,
                    GenerationStamp = "",
                    CityId = userDto.CityId,
                    Address=userDto.Address,
                    ImagePath=path.Result
                };

                var identityResult = await userManager.CreateAsync(user, userDto.Password);

                if (!identityResult.Succeeded)
                {
                    return operation.SetFailed(String.Join(",", identityResult.Errors.Select(error => error.Description)));
                }

                var Roles = new List<string> { userDto.UserType.ToString() };

                Roles.Add(UserType.User.ToString());

                IdentityResult roleIdentityResult = await userManager.AddToRolesAsync(user, Roles);

                if (!roleIdentityResult.Succeeded)
                {
                    return operation.SetFailed(String.Join(",", roleIdentityResult.Errors.Select(error => error.Description)));
                }

                await context.SaveChangesAsync();
                return operation.SetSuccess(await FillAccount(user, true));
            }
            catch (Exception ex)
            {
                return operation.SetException(ex);
            }

        }

        public async Task<OperationResult<IEnumerable<GetUserDto>>> GetAll()
        {
            var operation = new OperationResult<IEnumerable<GetUserDto>>();

            try
            {
                var user = await context.Users
                                        .Select(user => new GetUserDto
                                        {
                                            Id = user.Id,
                                            UserName=user.UserName,
                                            FirstName=user.FirstName,
                                            LastName=user.LastName,
                                            Email=user.Email,
                                            PhoneNumber=user.PhoneNumber,
                                            CityId=user.CityId,
                                            CityName=user.City.Name,
                                            Address=user.Address,
                                            ImagePath=user.ImagePath
                                        }).ToListAsync();

                return operation.SetSuccess(user);
            }
            catch (Exception ex)
            {
                return operation.SetException(ex);
            }
        }

        public async Task<OperationResult<GetUserDto>> Login(LoginDto loginDto)
        {
            var operation = new OperationResult<GetUserDto>();
            try
            {
                var user = await userManager.FindByNameAsync(loginDto.UserName);

                if (user is null)
                {
                    return operation.SetFailed($"{loginDto.UserName} Not Found", OperationResultType.Forbidden);
                }
                var loginResult = await signInManager.CheckPasswordSignInAsync(user, loginDto.Password, false);

                if (loginResult == SignInResult.Success)
                {
                    var account = await FillAccount(user, true);
                    return operation.SetSuccess(account);
                }
                else
                {
                    return operation.SetFailed("UnCorrect Password", OperationResultType.Forbidden);
                }

            }
            catch (Exception ex)
            {
                return operation.SetException(ex);
            }
        }

        public async Task<OperationResult<TokenDto>> RefreshToken(Guid id, string refreshToken)
        {
            var operation = new OperationResult<TokenDto>();
            try
            {
                var user = await userManager.FindByIdAsync(id.ToString());
                if (user is null)
                    return operation.SetFailed("user not found.", OperationResultType.Forbidden);

                if (!user.PasswordHash.Equals(refreshToken))
                    return operation.SetFailed("Invalid Refresh Token", OperationResultType.Forbidden);

                var roles = await userManager.GetRolesAsync(user);
                var expierDate = DateTime.Now.AddMinutes(GlobalValue.DefaultExpireTokenMinut);

                return operation.SetSuccess(new TokenDto()
                {
                    Token = GenerateJwtToken(user, roles, expierDate),
                    RefreshToken = user.PasswordHash,
                });
            }
            catch (Exception ex)
            {
                return operation.SetException(ex);
            }
        }

        #region Helper Methods 
        private async Task<GetUserDto> FillAccount(User user, bool isNewGeneration = false)
        {
            var roles = await userManager.GetRolesAsync(user);
            var expierDate = DateTime.Now.AddMinutes(GlobalValue.DefaultExpireTokenMinut);

            if (isNewGeneration)
            {
                user.GenerationStamp = Guid.NewGuid().ToString();
                await userManager.UpdateAsync(user);
            }

            var account = new GetUserDto()
            {
                Id = user.Id,
                FirstName = user.FirstName,
                LastName = user.LastName,
                UserName = user.UserName,
                Email = user.Email,
                PhoneNumber = user.PhoneNumber,
                CityId = user.CityId,
                CityName = context.Cities.Where(c => c.Id.Equals(user.CityId)).SingleOrDefault().Name,
                ImagePath = user.ImagePath,
                Token = GenerateJwtToken(user, roles, expierDate),
                RefreshToken = user.PasswordHash,
            };

            return account;
        }

        private string GenerateJwtToken(User user, IList<string> roles, DateTime expierDate)
        {
            var claims = new List<Claim>
            {
                new Claim(ClaimTypes.NameIdentifier, user.Id.ToString()),
                new Claim("generate-date", DateTime.Now.ToLocalTime().ToString()),
                new Claim("generation-stamp", user.GenerationStamp),
                new Claim("Type", user.UserType.ToString()),
            };

            foreach (var role in roles)
            {
                claims.Add(new Claim(ClaimTypes.Role, role));
            }

            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(configuration["Jwt:secret"]));

            var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256Signature);

            var token = new JwtSecurityToken(configuration["Jwt:validIssuer"],
                                             configuration["Jwt:validAudience"],
                                             claims,
                                             expires: expierDate,
                                             signingCredentials: creds);
            return new JwtSecurityTokenHandler().WriteToken(token);
        }
        #endregion
    }
}
